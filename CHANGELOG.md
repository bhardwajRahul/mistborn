# Mistborn Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project strives to adhere to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v2.4.0] - 2024-06-10

### Upgrades
- Upgraded to Python 3.12
- Upgraded Django and dependencies (and made it easy to do upgrade+test in the future)
- Rust toolchains supported for builds (including 32-bit ARM)
- Upgraded PostgreSQL to 16

### Added
- Automatic PostgreSQL database upgrades (between major versions) added to update.sh process
- All shared libraries that are dependencies of installed python modules are brought over from the build process

## [v2.3.3] - 2024-05-31

### Updated
- Python 3.8 preparations for upgrade

## [v2.3.2] - 2024-05-29

### Fixed
- Updated JavaScript dependencies

### Updated
- Pihole version 2024.05.0

## [v2.3.1] - 2024-05-07

### Fixed
- Ports 53, 443 redirect rules only apply for the shared WireGuard server instance

## Removed
- Custom port selection for WireGuard (obsolete)

## [v2.3.0] - 2024-04-19

### Added
- **WireGuard through firewalls**
    - UDP
        - Port 53: DNS is usually allowed through firewalls
        - Port 443: Used by the QUIC protocol, UDP/443 is becoming more commonly allowed through firewalls
- Added `Firewall Penetration` to README.md to explain how to use these alternate ports to connect to Mistborn

### Fixed
- Slimmed down the container size (multi-stage build process)

## [v2.2.3] - 2024-04-03

### Added
- Automated translation compilation

### Fixed
- Update status check

### Updated
- Pihole version 2024.03.2

### Removed
- Unused python modules

## [v2.2.1] - 2024-03-24

### Added
- First translations for German, French, Portuguese, Spanish, Dutch, Russian, Ukrainian, Arabic, Farsi, Japanese, Korean.
- Added update status check

### Fixed
- Update and restart processes
- Updated JavaScript dependencies

## [v2.2.0] - 2024-03-21

### Added
- Groups and Group Membership Management from WireGuard menu
- Single Sign On (OAuth2) Provider

### Fixed
- Updated JavaScript dependencies

### Removed
- Gateways removed from WireGuard management

## [v2.1.1] - 2024-03-14

### Fixed
- Sync server WireGuard confs with MFA clients (backward compatibility for Mistborn)

## [v2.1.0] - 2024-03-13

### Added
- WireGuard now uses the same listening port for new clients. Putting Mistborn behind a router with port forwarding becomes trivial.
- `mistborn-cli getconf` will now also show the qrencoded conf file.
- Translations have begun for German, Portuguese, French, Spanish.

### Fixed
- Handling sessions in a browser previously connected with a different Mistborn profile.

